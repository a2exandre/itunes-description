iTunes Description Tool (alpha)
==================================

Write better iOS app description on iTunes / App Store

★ Demo link : http://a2ex.cc/tools/ios/itunes-description/

✔ Download source : https://github.com/alexseo/itunes-description/archive/master.zip

❤ Feedback needed : https://twitter.com/a2exandre
