// ...
$(document).ready(function(){

	// click to submit or press enter to scroll down 
	$('#btn-results').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
	
	// up/down app screenshots
	$("h3.descriptionTitle").click(function(){
		$(".container-photos").slideDown();
		$(".container-media").css("height","550px");
	});
	$(".photos").click(function(){
		$(".container-media").css("height","48px");
		$(".container-photos").hide();
	});
	
	/* ///////////////////////////////////////////////
					USER FORM PREVIEW					
	/////////////////////////////////////////////// */
	
	// user description app form
	$("#appTitle").keyup(function() {
	  var value1 = $("#appTitle").val();
	  $("#value1").html(value1);
	  $("#iosValue1").html(value1);
	});
	
	$("#editorName").keyup(function() {
	  var value2 = $("#editorName").val();
	  $("#value2").html(value2);
	  $("#value6").html(value2);
	  $("#iosValue2").html(value2);
	});
	
	$("#baselineContent").keyup(function() {
	  var value3 = $("#baselineContent").val();
	  $("#value3").html(value3);
	});
	
	$("#feature-1").keyup(function() {
	  var value4 = $("#feature-1").val();
	  $("#value4").html(value4);
	});
	
	$("#feature-2").keyup(function() {
	  var value5 = $("#feature-2").val();
	  $("#value5").html(value5);
	});
	
	$("#feature-3").keyup(function() {
	  var value7 = $("#feature-3").val();
	  $("#value7").html(value7);
	});
	
	$("#update-version").keyup(function() {
	  var value8 = $("#update-version").val();
	  $("#value8").html(value8);
	});
	
	$("#news-update-baseline").keyup(function() {
	  var value9 = $("#news-update-baseline").val();
	  $("#value9").html(value9);
	});
	
	$("#feature-news-1").keyup(function() {
	  var value10 = $("#feature-news-1").val();
	  $("#value10").html(value10);
	});
	
	$("#feature-news-2").keyup(function() {
	  var value11 = $("#feature-news-2").val();
	  $("#value11").html(value11);
	});
	
	$("#feature-news-3").keyup(function() {
	  var value12 = $("#feature-news-3").val();
	  $("#value12").html(value12);
	});

	// click on "...Read more"
	$('.readMoreDescriptionLink.first').click(function() {
		$(this).hide();
		$('#value7').show();
		return false;
	});
	
	// baseline second
	$('.readMoreDescriptionLink.moreNewsFeatures').click(function() {
		$(this).hide();
		$('#value12').show();
		return false;
	});
	
	// dropzone help 512 x 512
	$('#dropzone').tooltip();
	
	$('#iosPreview').tooltip();
	
	// github fork ribbon affix bootstrap
	 $('#github').affix();	
	
	/* ///////////////////////////////////////////////
			MENU BAR / SOCIAL LINKS ANIMATION			
	/////////////////////////////////////////////// */
	
	$('.navigation.menu-bar-fake').click(function(){
		$(this).hide();
		$('#likeBtn .fb-like').show();
	});
	
	$('#likeBtn .fb-like').click(function(){
		$(this).hide();
		$('.navigation.menu-bar-fake').show();
	});
	
	$('#u_0_7').click(function(){
		$(this).hide();
		$('.navigation.menu-bar-fake').show();
	});

	
	/* ///////////////////////////////////////////////
					LOCAL STORAGE					
	/////////////////////////////////////////////// */

	if (localStorage) {
	 /* if form field values exist in local storage use
	 them to populate the form when the page loads */
	 
	 // demo url : http://www.simonbingham.me.uk/index.cfm/main/post/uuid/using-html5-local-storage-and-jquery-to-persist-form-data-47
	// !!!!!! don't waste your time, check you have "name" attribute for each form inputs !!!!!!
	
	if (localStorage.appTitle) {
	  $("#appTitle").val(localStorage.appTitle);
	  $("#value1").html(localStorage.appTitle);
	  // app name ios preview
	  $("#iosValue1").html(localStorage.appTitle);
	}
	
	if (localStorage.editorName) {
	  $("#editorName").val(localStorage.editorName);
	  $("#value2").html(localStorage.editorName);
	  // website anchor
	  $("#value6").html(localStorage.editorName);
	  // editor ios preview
	  $("#iosValue2").html(localStorage.editorName);
	}
	
	if (localStorage.baselineContent) {
	  $("#baselineContent").val(localStorage.baselineContent);
	  $("#value3").html(localStorage.baselineContent);
	}
	
	if (localStorage.feature1) {
	  $("#feature-1").val(localStorage.feature1);
	  $("#value4").html(localStorage.feature1);
	}
	
	if (localStorage.feature2) {
	  $("#feature-2").val(localStorage.feature2);
	  $("#value5").html(localStorage.feature2);
	}
	
	if (localStorage.feature3) {
	  $("#feature-3").val(localStorage.feature3);
	  $("#value7").html(localStorage.feature3);
	}
	
	if (localStorage.vnumber) {
	  $("#update-version").val(localStorage.vnumber);
	  $("#value8").html(localStorage.vnumber);
	}
	
	if (localStorage.updateBaseline) {
	  $("#news-update-baseline").val(localStorage.updateBaseline);
	  $("#value9").html(localStorage.updateBaseline);
	}
	
	if (localStorage.featureNews1) {
	  $("#feature-news-1").val(localStorage.featureNews1);
	  $("#value10").html(localStorage.featureNews1);
	}
	
	if (localStorage.featureNews2) {
	  $("#feature-news-2").val(localStorage.featureNews2);
	  $("#value11").html(localStorage.featureNews2);
	}
	
	if (localStorage.featureNews3) {
	  $("#feature-news-3").val(localStorage.featureNews3);
	  $("#value12").html(localStorage.featureNews3);
	}
	
	

	 /* When a form field changes store it's value in local storage */
	
		$("input[type=text]").change(function(){
		  $this = $(this);
		  localStorage[$this.attr("name")] = $this.val();
		});
		
		$("textarea").change(function(){
		  $this = $(this);
		  localStorage[$this.attr("name")] = $this.val();
		});

		$("input[type=checkbox]").change(function(){
		  $this = $(this);
		  localStorage[$this.attr("name")] = $this.attr("checked");
		});

		$("#userForm").change(function(){
		    console.log(localStorage);
		});

	};
	
	
	/* flush local storage on reset */
	$("#userForm #resetLocalStorage").click(function(){
		$('body,html').scrollTop(0);
		localStorage.clear();
		location.reload();
		return false;
	});
	
	
	/* Toggle hide / show mobile preview */
	$('#iosPreview').hide();
	
	$('#hideShowMobile').click(function(){
		$('#iosPreview').show();
		$('#hideShowMobile').html('');
	});
	
	$('#iosPreview').click(function(){
		$('#iosPreview').hide();
		$('#hideShowMobile').html('<i class="icon-eye-open"></i> show iOS preview');
	});
	
	
	
// ... end of $(document).ready(function() { ... });
});